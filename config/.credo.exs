%{
  configs: [
    %{
      name: "default",
      files: %{
        included: ["lib/", "example/"],
        excluded: []
      },
    }
  ]
}
