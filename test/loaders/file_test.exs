defmodule Weave.Loaders.FileTest do
  @moduledoc """
  Test Suite for the Weave File Loader
  """
  use ExUnit.Case, async: true
  alias Weave.Loaders.File, as: Loader

  defmodule Handler do
    @moduledoc nil
    use Weave

    def handle_configuration(key, value) do
      require Logger
      Logger.debug(fn -> "Handling #{key} and #{value}" end)

      Application.put_env(:weave_file_loader_test, key, value)
    end
  end

  setup_all do
    # Generate a random directory for the data needed
    directory = System.tmp_dir!() <> "/" <> UUID.uuid4()
    File.mkdir!(directory)

    # Generate 10 random file names and store a random value in each
    files =
      Enum.map(1..2, fn _ ->
        key = UUID.uuid4()

        filename =
          directory
          |> Kernel.<>("/")
          |> Kernel.<>(key)

        value = UUID.uuid4()

        File.write!(filename, value)

        {key, value}
      end)

    on_exit(fn ->
      File.rm_rf!(directory)
    end)

    {:ok, %{directory: directory, files: files}}
  end

  test ~s/it won't crash if the loader is called without configuration/ do
    assert [] == Loader.load_configuration(options: [], only: nil, handler: nil)
  end

  test ~s/it can be configured with only filter/ do
    File.write!(System.tmp_dir!() <> "/only_1", "value_1")
    File.write!(System.tmp_dir!() <> "/only_2", "value_2")

    # Case shouldn't matter when configuring only filter
    assert ["only_1"] =
             Loader.load_configuration(
               options: [directories: [System.tmp_dir!()]],
               only: ["ONLY_1"],
               handler: nil
             )

    assert ["only_1"] =
             Loader.load_configuration(
               options: [directories: [System.tmp_dir!()]],
               only: ["only_1"],
               handler: nil
             )
  end

  test ~s/it can load configuration from a single directory/, %{
    directory: directory,
    files: files
  } do
    assert :ok =
             Handler.configure(
               loaders: [
                 {Weave.Loaders.File,
                  [
                    directories: [
                      directory
                    ]
                  ]}
               ]
             )

    Enum.each(files, fn {key, value} ->
      assert value == Application.get_env(:weave_file_loader_test, key)
    end)
  end
end
