defmodule Weave.Loaders.EnvironmentTest do
  @moduledoc """
  Test Suite for the Weave Environment Loader
  """
  use ExUnit.Case, async: true
  alias Weave.Loaders.Environment, as: Loader

  defmodule Handler do
    @moduledoc nil
    use Weave

    def handle_configuration(key, value) do
      Application.put_env(:weave_environment_loader_test, key, value)
    end
  end

  setup_all do
    # Generate Environment Prefix
    prefix = String.replace(UUID.uuid4(), "-", "")

    # Generate 10 random keys and store a random value in each
    config =
      Enum.map(1..2, fn _ ->
        key = String.replace(UUID.uuid4(), "-", "")
        value = UUID.uuid4()

        prefix
        |> Kernel.<>(key)
        |> System.put_env(value)

        {key, value}
      end)

    {:ok, %{prefix: prefix, config: config}}
  end

  test ~s/it won't crash if the loader is called without configuration/ do
    assert [] == Loader.load_configuration(options: [], only: nil, handler: nil)
  end

  test ~s/it can be configured with a nil prefix/ do
    loaded = Loader.load_configuration(options: [prefix: nil], only: nil, handler: nil)

    # Assuming every OS has a path ENV
    assert true = Enum.member?(loaded, "path")
  end

  test ~s/it can be configured with a prefix/ do
    System.put_env("TEST_1", "VALUE_1")

    assert ["1"] = Loader.load_configuration(options: [prefix: "TEST_"], only: nil, handler: nil)
  end

  test ~s/it can be configured with only filter/ do
    System.put_env("ONLY_1", "VALUE_1")

    # Case shouldn't matter when configuring only filter
    assert ["only_1"] =
             Loader.load_configuration(options: [prefix: nil], only: ["ONLY_1"], handler: nil)

    assert ["only_1"] =
             Loader.load_configuration(options: [prefix: nil], only: ["only_1"], handler: nil)
  end

  test ~s/it can load configuration from a single directory/, %{prefix: prefix, config: config} do
    assert :ok =
             Handler.configure(
               loaders: [
                 {Weave.Loaders.Environment,
                  [
                    prefix: prefix
                  ]}
               ]
             )

    Enum.each(config, fn {key, value} ->
      assert value == Application.get_env(:weave_environment_loader_test, key)
    end)
  end
end
