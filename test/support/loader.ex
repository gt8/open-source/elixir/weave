defmodule Weave.Loaders.Test do
  @moduledoc false
  use Weave.Loader

  def load_configuration(options: _, only: _, handler: _) do
    []
  end

  def test_apply_configuration(name, contents, handler) do
    apply_configuration(name, contents, handler)
  end

  def test_handle_configuration(parameter_name, configuration) do
    handle_configuration(parameter_name, configuration)
  end

  def test_merge(current_config, new_config) do
    merge(current_config, new_config)
  end

  def test_sanitize(key) do
    sanitize(key)
  end
end
