# Contributing

## GitLab && GitHub

The canonical repository for this project is hosted on [GitLab](https://gitlab.com/gt8/open-source/elixir/weave). This is a personal chocie of it's maintainers (Described in brief [here](https://github.com/GT8Online/weave/pull/42#issuecomment-364871547)).

While the maintainers feel this is the best choice for the project, they do not wish to exclude anyone from contributing. So please, if you prefer, feel free to use the GitHub mirror for cloning, pull-requests and issues. The maintainers will handle the awkward bits, so you don't have too.

## Docker

If you wish to use Docker, we've got you covered! Jump into a `dshell` and the rest of the `Makefile` targets can be used from there.

```dshell
make dshell
```

When you're done, you can clean up with `dclean`

## Developing

This project relies on it's tests. All new features must be accompanied by a test. While this project does make extensive use of `cabbage` and BDD scenarios, we will not reject a merge-request that opts for a standard `ExUnit` test.

In-order to run the tests, you can utilise your normal `Mix` commands, or use our `Makefile` targets:

```shell
make init deps compile test
```
