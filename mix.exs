defmodule Weave.Mixfile do
  @moduledoc """
  Weave: Elixir's JIT Configuration Module
  """
  use Mix.Project

  @non_prod_environments [:dev, :test]

  def project do
    [
      app: :weave,
      version: "4.0.0",
      elixir: "~> 1.3",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      elixirc_paths: elixirc_paths(Mix.env()),
      aliases: aliases(),
      description: description(),
      package: package(),
      test_coverage: [tool: ExCoveralls],
      docs: [main: "readme", extras: ["README.md"]]
    ]
  end

  def application do
    [
      applications: [
        :logger
      ]
    ]
  end

  def deps do
    [
      {:credo, "~> 0.8", only: @non_prod_environments, runtime: false},
      {:dialyxir, "~> 0.5", only: @non_prod_environments, runtime: false},
      {:ex_doc, ">= 0.0.0", only: @non_prod_environments},
      {:excoveralls, "~> 0.7", only: @non_prod_environments},
      {:uuid, "~> 1.1", only: @non_prod_environments}
    ]
  end

  def aliases do
    [init: ["local.hex --force", "deps.get"]]
  end

  defp description do
    """
    Weave allows you to configure your application just-in-time (JIT),
    embracing the 12-Factor manifesto. Weave supports loading configuration from
    environment variables and files, with prefix and
    filename filtering. Works great with Kubernetes and Docker Swarm.
    """
  end

  defp package do
    [
      name: :weave,
      files: ["lib", "mix.exs", "README.md", "LICENSE"],
      maintainers: ["GT8 Online"],
      licenses: ["MIT"],
      links: %{"Source Code" => "https://gitlab.com/gt8/open-source/elixir/weave"}
    ]
  end

  defp elixirc_paths(:dev), do: ["lib"]
  defp elixirc_paths(:test), do: ["test", "test/support", "example"] ++ elixirc_paths(:dev)
  defp elixirc_paths(_), do: ["lib"]
end
